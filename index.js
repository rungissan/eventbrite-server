var express = require('express');
var nforce = require('nforce');
var parseHttpHeader = require('parse-http-header');

//var contentType = parseHttpHeader(response.headers['content-type'])[0];
// text/html 
//var charset = parseHttpHeader(response.headers['content-type'])['charset'];
// UTF-8 

var org = nforce.createConnection({
  clientId: '3MVG9HxRZv05HarRwBAlQYrwuE.x7hJD32DYJ6Iy5_LEpiwbjS6F62dFmykLz8OKr1E.mellhrffmtIKfNCqb',
  clientSecret: '8712797791172598052',
  redirectUri: 'https://login.salesforce.com/services/oauth2/authorize',
  //apiVersion: 'v27.0',  // optional, defaults to current salesforce API version 
  environment: 'production',  // optional, salesforce 'sandbox' or 'production', production default 
  mode: 'multi' // optional, 'single' or 'multi' user mode, multi default 
});
var oauth;
org.authenticate({ username: xxxxx, password:xxxxx}, function(err, response){
  // store the oauth object for this user 
 if (err) {  console.error(err);
                } else {
                        oauth = response;
                        console.log(oauth)
                      }
});

var bodyparser = require('body-parser');

var app = express();
// configure app to use body()
// this will let us get the data from a POST
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/public'));
app.get('/', function(request, response) {
       response.render('public/index');
    });

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

 //**********************************************************
 //***************** AUTHENTIFICATION SECTION ***********************

      router.route('/auth/sfdc')
         .post(function(req, res) {
         org.authenticate({ username: req.body.email, password:req.body.password}, function(err, resp){
        // store the oauth object for this user 
                  if (err) {  
                                 res.status(err.statusCode);
                                 res.json({ message: err }); 
                                  } else {
                                          var oauth_new = resp;
                                          console.log(oauth_new);
                                            var a = oauth_new.id.split('/')[5];
                                            console.log(a);
                                            var q = "SELECT Id, Name FROM User WHERE id='"+a+"'";
                   org.query({ query: q,oauth: oauth_new }, function(err, resp){
                  if(!err && resp) {
                                    var response=resp.records[0];
                                    res.json({response,oauth_new}); 
                                   } else {
                                           console.error(err);
                                           res.status(err.statusCode);
                                           res.json({ message: err });   
                                          }
                                               });
                                           }
                      });
 
        });

   //**********************************************************
  //***************** SESSIONS SECTION ***********************
    
      router.route('/sessions/delete/:id')
        //delete sessions
       .put(function(req, res) {
        var acc = nforce.createSObject('Sessions__c');
        var oauth_user = req.body.oauth;

        acc.set('id', req.body.id);
      
        
        console.log('delete Session start!!!');

        org.delete({ sobject: acc, oauth: oauth_user }, function(err, resp){
              if(!err) {               
                console.log('Session deleted!');
                res.json({ message: resp }); 
                          } else {
                                  console.error(err);
                                  res.status(err.statusCode);
                                  res.json({ message: err }); 
                                  }
                        });
                  
          });

     router.route('/sessions')
         //create Sessions__c  
        .post(function(req, res) {
        var acc = nforce.createSObject('Sessions__c');
        var oauth_user = req.body.oauth;

       // acc.set('Name', req.body.Name);
        acc.set('Subject__c', req.body.title);
        acc.set('Description__c', req.body.description);
        acc.set('StartDateTime__c', req.body.start);
        acc.set('EndDateTime__c', req.body.end);
        acc.set('EventID__c', req.body.eventid);
        
        console.log('started creating session!!!');
       

        org.insert({ sobject: acc, oauth: oauth_user }, function(err, resp){
              if(!err) {               
                console.log('Sessions created!');
                res.json({ message: resp }); 
                          } else {
                                  console.error(err);
                                  res.status(err.statusCode);
                                  res.json({ message: err }); 
                                  }
                        });
                  
          });

     router.route('/sessions/:id')
        // update Session
        .put(function(req, res) {
        var acc = nforce.createSObject('Sessions__c');
        
        var oauth_user = req.body.oauth; 
        acc.set('id', req.body.id);
        acc.set('Subject__c', req.body.title);
        acc.set('Description__c', req.body.description);
        acc.set('StartDateTime__c', req.body.start);
        acc.set('EndDateTime__c', req.body.end);
        
                      
        console.log('Session updating process!!!');
        console.log(acc);

        org.update({ sobject: acc, oauth: oauth_user }, function(err, resp){
              if(!err) {               
                console.log('Session updated!');
                res.json({ message: resp }); 
                          } else {
                                  console.error(err);
                                  res.status(err.statusCode);
                                  res.json({ message: err }); 
                                  }
                        });
                  
          })
        // get Sessions__c
       .get(function(req, res) {
          const ids = req.params.id;
                   
         var q = "SELECT Id, Name,Subject__c,Description__c,StartDateTime__c,EndDateTime__c,EventID__c FROM Sessions__c WHERE EventID__c='"+ids+"'";
            
              org.query({ query: q,oauth: oauth }, function(err, resp){
                  if(!err && resp.records) {
                    var response=resp.records;
                    res.json(response); 
                    } else {
                      console.error(err);
                      res.status(err.statusCode);
                      res.json({ message: err });   
                    }
            });
        });
 //**********************************************************
  //***************** EVENTS SECTION  in progress***********************
    
    router.route('/events/delete')
     //delete events
       .put(function(req, res) {
        var acc = nforce.createSObject('Event');
        var oauth_user = req.body.oauth;
        
        acc.set('name', req.body.id);
             
        console.log('start delete event!!!');
        org.delete({ sobject: acc, oauth: oauth_user }, function(err, resp){
              if(!err) {               
                console.log('Event deleted!');
                res.json({ message: resp }); 
                          } else {
                                  console.error(err);
                                  res.status(err.statusCode);
                                  res.json({ message: err }); 
                                  }
                        });
                  
          });
   
    router.route('/events')
     //create events
        .post(function(req, res) {
        var acc = nforce.createSObject('Event');
        var oauth_user = req.body.oauth;
       
        acc.set('Title__c', req.body.Title);
        acc.set('Description__c', req.body.Description);
        acc.set('StartDateTime__c', req.body.StartDateTime);
        acc.set('EndDateTime__c', req.body.EndDateTime);
        acc.set('Registration_Limit__c', req.body.RegistrationLimit);
        acc.set('Status__c', req.body.Status);
       
        console.log('started creating event!!!');

        org.insert({ sobject: acc, oauth: oauth_user }, function(err, resp){
              if(!err) {               
                console.log('Event created!');
                res.json({ message: resp }); 
                          } else {
                                  console.error(err);
                                  res.status(err.statusCode);
                                  res.json({ message: err }); 
                                  }
                        });
                  
          })
         // update events
        .put(function(req, res) {
        var acc = nforce.createSObject('Event');
        var oauth_user = req.body.oauth;
       
        acc.set('id', req.body.id);
        acc.set('Registration_Limit__c', req.body.Registration_Limit);
        console.log(acc);

              
        console.log('update event!!!');

        org.update({ sobject: acc, oauth: oauth_user }, function(err, resp){
              if(!err) {               
                console.log('Event updated!');
                res.json({ message: resp }); 
                          } else {
                                  console.error(err);
                                  res.status(err.statusCode);
                                  res.json({ message: err }); 
                                  }
                        });
                  
          })
        // get events
       .get(function(req, res) {
         var q = 'SELECT Id, Name,Title__c,Description__c,Remaining_Seats__c,Status__c,StartDateTime__c,EndDateTime__c,Registration_Limit__c FROM Event__c';
              org.query({ query: q,oauth: oauth }, function(err, resp){
                  if(!err && resp.records) {
                    var response=resp.records;
                    res.json(response); 
                    } else {
                      console.error(err);
                      res.status(err.statusCode);
                      res.json({ message: err });   
                    }
            });
        });
  //**********************************************************
  //***************** ATTENDEE SECTION ***********************
  router.route('/attendee')
     //create attendee
        .post(function(req, res) {
        var acc = nforce.createSObject('Attendee__c');
       
        acc.set('Firstname__c', req.body.firstname);
        acc.set('Lastname__c', req.body.lastname);
        acc.set('Company_Name__c', req.body.company);
        acc.set('Email__c', req.body.email);
        acc.set('Phone__c', req.body.phone);
        acc.set('Sessions__c', req.body.sessions);
        acc.set('EventID__c', req.body.eventid);
        console.log('started creating attendee!!!');

        org.insert({ sobject: acc, oauth: oauth}, function(err, resp){
              if(!err) {               
                console.log('Attendee created!');
                res.json({ message: resp }); 
                          } else {
                                  console.error(err);
                                  res.status(err.statusCode);
                                  res.json({ message: err }); 
                                  }
                        });
                  
          });
      
       
 
// test route to make sure everything is working (accessed at GET http://localhost:5000/api)
router.get('/', function(request, response) {
    response.json({ message: 'Hello! welcome EventsBrite api!' });   
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);


// START THE SERVER
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


